<?php

namespace App\EventSubscriber;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Security;

class PostSubscriber implements EventSubscriber
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->em = $entityManager;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        $user = $this->security->getUser();

        if ($entity instanceof Post && $user instanceof User) {
            $this->onPostUpdated($entity, $user);
        }
    }

    public function onPostUpdated(Post $post, User $user): void
    {
        $user->increaseNbPostsUpdated();

        $this->em->flush();
    }
}