<?php

namespace App\Controller\Admin;

use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user")
 * @IsGranted("ROLE_ADMIN")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", defaults={"page": "1"}, methods="GET", name="admin_user_index")
     * @Route("/page/{page<[1-9]\d*>}", methods="GET", name="admin_user_index_paginated")
     * @Cache(smaxage="10")
     * @param UserRepository $userRepository
     * @param int $page
     * @return Response
     */
    public function index(UserRepository $userRepository, int $page): Response
    {
        $users = $userRepository->findForIndex($page);

        return $this->render('admin/user/index.html.twig', [
            'paginator' => $users
        ]);
    }
}
