<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\User;
use App\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/doctrine.html#querying-for-objects-the-repository
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findForIndex(int $page = 1): Paginator
    {
        $qb = $this->createQueryBuilder('user');

        $qb
            ->select('user AS user_entity')
            ->addSelect(sprintf('%s AS nb_user_posts', $qb->expr()->count('posts')))
            ->addSelect(sprintf('%s AS nb_user_comments', $qb->expr()->count('comments')))
            ->leftJoin('user.posts', 'posts')
            ->leftJoin('user.comments', 'comments')
            ->groupBy('user.id');

        return (new Paginator($qb))->paginate($page);
    }
}
